/*
 * Copyright (C) 2021  Weiyang(Will) Xu
 *
 * This file is part of advanced-microservices.
 * Advanced-microservices is free software: you can redistribute it and/or modify
 * it under the term of the GNU General Public License version 3 or
 * any later version, as specified in the readme.md file.
 */

package org.willxu.eurekaserver;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

@EnableEurekaServer
@SpringBootApplication
public class EurekaSeverApplication {
    public static void main(String[] args) {
        new SpringApplicationBuilder(EurekaSeverApplication.class).web(WebApplicationType.SERVLET).run(args);
    }
}
